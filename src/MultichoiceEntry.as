package
{
	public class MultichoiceEntry
	{
	
		private var english:String;
		private var japanese:String
		private var id:int;
		private var data:Array;
		
		public function MultichoiceEntry(question:String, val:int)
		{
			data = question.split(",");
			
			english = data[0];
			japanese = data[1];
			id = val;
		
		}
		
		public function getEnglish():String
		{
			return this.english;
		}
		
		public function getJapanese():String
		{
			return this.japanese;
		}
		
		public function getID():int
		{
			return this.id;
		}
		
		
		
	}
}
package
{
	public class DocchiQuestion
	{
		private var quest:String;
		private var ans:String;
		private var a:String;
		private var b:String;
		private var c:String;
		private var d:String;
		private var array:Array;
		
		public function DocchiQuestion(question:String)
		{
			init(question);
		}
		
		private function init(question:String):void
		{
			array = question.split(",");
			quest = array[0];
			ans = array[1];
			a = array[2];
			b = array[3];

			
				
		}	
		
		public function toString():String
		{
		  var string:String = quest + ans + a + b;
		  return string;
		}
		
		public function getQuestion():String
		{
			return quest;
		}
		
		public function getAns():String
		{
			return ans;
		}
		
		public static function randomRange(low:Number, high:Number):int
		{
			return Math.floor(Math.random() * (high - low)) + low;
		}	
		
		public function getA():String
		{
			return a;
		}
		public function getB():String
		{
			return b;
		}
		
		
	}
}